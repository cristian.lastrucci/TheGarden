function getPlant( treeType ){
	// Carico i parametri di costruzione in base al tipo di albero
	var treeAxiom = loadTreeAxiom(treeType);
	var treeRules = loadTreeRules(treeType);
	var treeParams = loadTreeInitialParams(treeType);
	
	// Determino la struttura dell'albero
	var treeStructure = getTreeStructure( treeAxiom, treeRules, treeParams.iterations );
	
	// Carico la texture dell'albero
	var treeMaterial = loadTreeMaterial(treeType);
	
	// Carico la texture delle foglie
	var leafMaterial = loadLeafMaterial(treeType);
	
	// Costruisco l'albero
	var tree = buildTree( treeStructure, treeParams, treeMaterial, leafMaterial, treeRules, treeType);
	
	return tree;
}


function loadTreeAxiom( treeType ){
	var treeAxiom;
	if(treeType == "dryTree"){
		treeAxiom = "T";
	}else if(treeType == "treeWithLeaves"){
		treeAxiom = "T";
	}else if(treeType == "dryFir"){
		treeAxiom = "T";
	}else if(treeType == "firWithLeaves"){
		treeAxiom = "T";
	}else if(treeType == "bush"){
		treeAxiom = "B";
	}else{
		treeAxiom = "FFFFF";
	}
	return treeAxiom;
}


function loadTreeRules( treeType ){
	var treeRules = {};
	if(treeType == "dryTree"){
		treeRules.T = [];
		treeRules.T.push("FFF[+R][--R][>>R][<<R]F[++R][-R]F[>R][<<R]F[F++][F--]");
		treeRules.R = [];
		treeRules.R.push("F[+R][-R]R");
		treeRules.R.push("F[>R][<R]R");
		treeRules.R.push("F[+<R][->R]R");
		treeRules.R.push("F[>R][++R]R");
		treeRules.R.push("F[->>R][---R]R");
		treeRules.R.push("F[>R][<<R]R");
		treeRules.R.push("F[+<R][->R]R");
		treeRules.R.push("F[++>R][<++R]R");
		treeRules.R.push("F[++R]R");
		treeRules.R.push("F[--R]R");
		treeRules.R.push("F[>>R]R");
		treeRules.R.push("F[<<R]R");
		treeRules.R.push("FR");
		treeRules.R.push("F[+R][-R]R");
	}else if(treeType == "treeWithLeaves"){
		treeRules.T = [];
		treeRules.T.push("FFF[+R][--R][>>R][<<R]F[++R][-R]F[>R][<<R]F[F++][F--]");
		treeRules.R = [];
		treeRules.R.push("F[+R][-R]R");
		treeRules.R.push("F[>R][<R]R");
		treeRules.R.push("F[+<R][->R]R");
		treeRules.R.push("F[>R][++R]R");
		treeRules.R.push("F[->>R][---R]R");
		treeRules.R.push("F[>R][<<R]R");
		treeRules.R.push("F[+<R][->R]R");
		treeRules.R.push("F[++>R][<++R]R");
		treeRules.R.push("F[++R]R");
		treeRules.R.push("F[--R]R");
		treeRules.R.push("F[>>R]R");
		treeRules.R.push("F[<<R]R");
		treeRules.R.push("FR");
		treeRules.R.push("F[+R][-R]R");
		treeRules.L = [];
		treeRules.L.push("F[+++F[+L][-L][>L][<L]F[+>L][-<L][>>L][+<L]][---F[+L][-L][>L][<L]F[+>L][-<L][>>L][+<L]][>>>F[+L][-L][>L][<L]F[+>L][-<L][>>L][+<L]][<<<F[+L][-L][>L][<L]F[+>L][-<L][>>L][+<L]]F[++F[+L][-L][>L][<L]F[+>L][-<L][>>L][+<L]][--F[+L][-L][>L][<L]F[+>L][-<L][>>L][+<L]][>>F[+L][-L][>L][<L]F[+>L][-<L][>>L][+<L]][<<F[+L][-L][>L][<L]F[+>L][-<L][>>L][+<L]]");
	}else if(treeType == "dryFir"){
		treeRules.T = [];
		treeRules.T.push("FFFFFF[+++++A][-----B][>>>>>C][<<<<<D]FF[+++++E][-----G][>>>>>H][<<<<<I]FF[+++++J][-----K][>>>>>M][<<<<<N]FF[+++++O][-----P][>>>>>Q][<<<<<S]F");
		treeRules.A = [];
		treeRules.A.push("FFFF-A");
		treeRules.B = [];
		treeRules.B.push("FFFF+B");
		treeRules.C = [];
		treeRules.C.push("FFFF<C");
		treeRules.D = [];
		treeRules.D.push("FFFF>D");
		treeRules.E = [];
		treeRules.E.push("FFF[-E]");
		treeRules.G = [];
		treeRules.G.push("FFF[+G]");
		treeRules.H = [];
		treeRules.H.push("FFF[<H]");
		treeRules.I = [];
		treeRules.I.push("FFF[>I]");
		treeRules.J = [];
		treeRules.J.push("FF[-J]");
		treeRules.K = [];
		treeRules.K.push("FF[+K]");
		treeRules.M = [];
		treeRules.M.push("FF[<M]");
		treeRules.N = [];
		treeRules.N.push("FF[>N]");
		treeRules.O = [];
		treeRules.O.push("F[-O]");
		treeRules.P = [];
		treeRules.P.push("F[+P]");
		treeRules.Q = [];
		treeRules.Q.push("F[<Q]");
		treeRules.S = [];
		treeRules.S.push("F[>S]");
	}else if(treeType == "firWithLeaves"){
		treeRules.T = [];
		treeRules.T.push("FFFFFF[+++++A][-----B][>>>>>C][<<<<<D]FF[+++++E][-----G][>>>>>H][<<<<<I]FF[+++++J][-----K][>>>>>M][<<<<<N]FF[+++++O][-----P][>>>>>Q][<<<<<S]F");
		treeRules.A = [];
		treeRules.A.push("F[R]F[R]F[R]F[R]-A");
		treeRules.B = [];
		treeRules.B.push("F[R]F[R]F[R]F[R]+B");
		treeRules.C = [];
		treeRules.C.push("F[R]F[R]F[R]F[R]<C");
		treeRules.D = [];
		treeRules.D.push("F[R]F[R]F[R]F[R]>D");
		treeRules.E = [];
		treeRules.E.push("F[R]F[R]F[R][-E]");
		treeRules.G = [];
		treeRules.G.push("F[R]F[R]F[R][+G]");
		treeRules.H = [];
		treeRules.H.push("F[R]F[R]F[R][<H]");
		treeRules.I = [];
		treeRules.I.push("F[R]F[R]F[R][>I]");
		treeRules.J = [];
		treeRules.J.push("F[R]F[R][-J]");
		treeRules.K = [];
		treeRules.K.push("F[R]F[R][+K]");
		treeRules.M = [];
		treeRules.M.push("F[R]F[R][<M]");
		treeRules.N = [];
		treeRules.N.push("F[R]F[R][>N]");
		treeRules.O = [];
		treeRules.O.push("F[R][-O]");
		treeRules.P = [];
		treeRules.P.push("F[R][+P]");
		treeRules.Q = [];
		treeRules.Q.push("F[R][<Q]");
		treeRules.S = [];
		treeRules.S.push("F[R][>S]");
		treeRules.L = [];
		treeRules.L.push("[+++F[++L][--L]F[++L][<<L]F[--L][<<L]F[++L][>>L]F[--L][<<L]F[++L][<<L]F[<<L][>>L]F[++L][--L]F[++L][<<L]F[--L][<<L]F[++L][>>L]F[--L][<<L]F[++L][<<L]F[<<L][>>L]][---F[++L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[<<L][>>L]F[++L][<<L]F[--L][<<L]F[++L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[<<L][>>L]F[++L][<<L]F[--L][<<L]]");
		treeRules.L.push("[---F[++L][<<L]F[<<L][>>L]F[++L][>>L]F[++L][>>L]F[<<L][>>L]F[++L][>>L]F[--L][<<L]F[++L][--L]F[++L][<<L]F[--L][<<L]F[++L][>>L]F[--L][<<L]F[++L][<<L]F[<<L][>>L]][+++F[<<L][>>L]F[<<L][>>L]F[++L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[<<L][>>L]F[++L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[<<L][>>L]F[++L][<<L]F[--L][<<L]]");
		treeRules.L.push("[>>>F[++L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[<<L][>>L]F[++L][<<L]F[--L][<<L]F[--L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][<<L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]][<<<F[++L][--L]F[++L][<<L]F[--L][<<L]F[++L][>>L]F[--L][<<L]F[++L][<<L]F[<<L][>>L]F[<<L][>>L]F[<<L][>>L]F[++L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[<<L][>>L]]");
		treeRules.L.push("[<<<F[--L][<<L]F[++L][>>L]F[++L][<<L]F[--L][<<L]F[++L][>>L]F[--L][<<L]F[++L][<<L]F[++L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[<<L][>>L]F[++L][<<L]F[--L][<<L]][>>>F[++L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[<<L][>>L]F[++L][<<L]F[--L][<<L]F[<<L][>>L]F[<<L][>>L]F[++L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[<<L][>>L]]]");
		treeRules.L.push("[+++F[--L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][<<L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[++L][<<L]F[<<L][>>L]F[++L][>>L]F[++L][>>L]F[<<L][>>L]F[++L][>>L]F[--L][<<L]][---F[++L][<<L]F[<<L][>>L]F[++L][>>L]F[++L][>>L]F[<<L][>>L]F[++L][>>L]F[--L][<<L]F[++L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[<<L][>>L]F[++L][<<L]F[--L][<<L]]");
		treeRules.L.push("[<<<F[<<L][>>L]F[<<L][>>L]F[++L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[<<L][>>L]F[++L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[<<L][>>L]F[++L][<<L]F[--L][<<L]][>>>F[++L][--L]F[++L][<<L]F[--L][<<L]F[++L][>>L]F[--L][<<L]F[++L][<<L]F[<<L][>>L]F[<<L][>>L]F[<<L][>>L]F[++L][>>L]F[--L][<<L]F[<<L][>>L]F[++L][>>L]F[<<L][>>L]]");
	}else if(treeType == "bush"){
		treeRules.B = [];
		treeRules.B.push("[++FB][--FB][>>FB][<<FB][FB]");
	}else{
		treeRules.A = [];
		treeRules.A.push("F[++A][--A]>>>A");
	}
	return treeRules;
}

function loadTreeMaterial( treeType ){
	
	var basePath = "textures/";
	var texturePath;
	
	if(treeType == "dryTree") {
		texturePath = basePath + "treeWood.jpg";
	}else if(treeType == "treeWithLeaves") {
		texturePath = basePath + "treeWood.jpg";
	}else if(treeType == "dryFir") {
		texturePath = basePath + "firWood.jpg";
	}else if(treeType == "firWithLeaves") {
		texturePath = basePath + "firWood.jpg";
	}else if(treeType == "bush") {
		texturePath = basePath + "bush.jpg";
	}else{
		texturePath = basePath + "treeWood.jpg";
	}
	
	var treeTexture = new THREE.TextureLoader().load( texturePath );
	return new THREE.MeshPhongMaterial( { map: treeTexture, side: THREE.DoubleSide } );
}


function loadLeafMaterial( treeType ){
	
	var basePath = "textures/";
	var texturePath;
	
	if(treeType == "treeWithLeaves") {
		texturePath = basePath + "leaf.jpg";
	}else if(treeType == "firWithLeaves") {
		texturePath = basePath + "firLeaf.jpg";
	}else{
		texturePath = basePath + "pineLeaf.jpg";
	}
	
	if(texturePath.includes("textures/")){
		var leafTexture = new THREE.TextureLoader().load( texturePath );
		return new THREE.MeshPhongMaterial( { map: leafTexture, side: THREE.DoubleSide } );
	}else{
		return new THREE.MeshPhongMaterial( { color: texturePath, side: THREE.DoubleSide } );
	}
}


function loadTreeInitialParams(treeType){
	var treeInitialParams = {};
	if(treeType == "dryTree"){
		treeInitialParams.iterations = 5;
		treeInitialParams.branchAngle = 27;
		treeInitialParams.branchMaxRadius = 15;
		treeInitialParams.branchReduction = 20;
		treeInitialParams.branchMinRadius = treeInitialParams.branchMaxRadius - treeInitialParams.branchMaxRadius*treeInitialParams.branchReduction/100;
		treeInitialParams.branchLenght = 120;
		treeInitialParams.position = new THREE.Vector3( 0, 0, 0 );
		treeInitialParams.rotation = new THREE.Quaternion();	
	}else if(treeType == "treeWithLeaves"){
		treeInitialParams.iterations = 5;
		treeInitialParams.branchAngle = 33;
		treeInitialParams.branchMaxRadius = 15;
		treeInitialParams.branchReduction = 20;
		treeInitialParams.branchMinRadius = treeInitialParams.branchMaxRadius - treeInitialParams.branchMaxRadius*treeInitialParams.branchReduction/100;
		treeInitialParams.branchLenght = 120;
		treeInitialParams.position = new THREE.Vector3( 0, 0, 0 );
		treeInitialParams.rotation = new THREE.Quaternion();	
	}else if(treeType == "dryFir" || treeType == "firWithLeaves"){
		treeInitialParams.iterations = 4;
		treeInitialParams.branchAngle = 27;
		treeInitialParams.branchMaxRadius = 25;
		treeInitialParams.branchReduction = 12;
		treeInitialParams.branchMinRadius = treeInitialParams.branchMaxRadius - treeInitialParams.branchMaxRadius*treeInitialParams.branchReduction/100;
		treeInitialParams.branchLenght = 50;
		treeInitialParams.position = new THREE.Vector3( 0, 0, 0 );
		treeInitialParams.rotation = new THREE.Quaternion();	
	}else if(treeType == "bush"){
		treeInitialParams.iterations = 4;
		treeInitialParams.branchAngle = 21;
		treeInitialParams.branchMaxRadius = 3;
		treeInitialParams.branchReduction = 5;
		treeInitialParams.branchMinRadius = treeInitialParams.branchMaxRadius - treeInitialParams.branchMaxRadius*treeInitialParams.branchReduction/100;
		treeInitialParams.branchLenght = 15;
		treeInitialParams.position = new THREE.Vector3( 0, 0, 0 );
		treeInitialParams.rotation = new THREE.Quaternion();
	}else{
		treeInitialParams.iterations = 4;
		treeInitialParams.branchAngle = 25;
		treeInitialParams.branchMaxRadius = 5;
		treeInitialParams.branchReduction = 20;
		treeInitialParams.branchMinRadius = treeInitialParams.branchMaxRadius * treeInitialParams.branchReduction/100;
		treeInitialParams.branchLenght = 80;
		treeInitialParams.position = new THREE.Vector3( 0, 0, 0 );
		treeInitialParams.rotation = new THREE.Quaternion();
	}
	return treeInitialParams;
}


function getTreeStructure( axiom, rules, iterations ){
	var sequence = axiom;
	var currentSequence;
	
	for( var i=0; i<iterations; i++ ){
		// Per ogni iterazione
		currentSequence = "";
		for( var j=0; j<sequence.length; j++){
			// Per ogni carattere della sequenza
			var rule = getRandomRule(rules, sequence.charAt(j));
			if( rule !== "" ){
				// Se c'è una regola di evoluzione del carattere corrente la applico
				currentSequence = currentSequence + rule;
			}else{
				// Altrimenti ricopio il carattere originale
				currentSequence = currentSequence + sequence.charAt(j);
			}
		}
		sequence = currentSequence;
	}
	
	return sequence;
}


function getRandomRule(allRules, char){
	if(allRules[char] !== undefined){
		var charRules = allRules[char];
		if(charRules.length > 1){
			var randomPos = Math.floor(Math.random()*charRules.length);
			return charRules[randomPos];
		}else{
			return charRules[0];	
		}
	}
	
	return "";
}


function extractTreeSubStructure( structure, startIndex ){
	var i = startIndex;
	var subStructureEndFound = false;
	var subStructure = "";
	var subTreesOpen = 0;
	while( i<structure.length && subStructureEndFound == false){
		if(structure.charAt(i) == "]" && subTreesOpen==0){
			subStructureEndFound = true;
		}else{
			if(structure.charAt(i) == "["){
				subTreesOpen += 1;
			}else if(structure.charAt(i) == "]"){
				subTreesOpen -= 1;
			}
			subStructure = subStructure + structure.charAt(i);
			i++;	
		}
	}
	return subStructure;
}


function buildTree( structure, initialParams, material, leafMaterial, treeRules, treeType ){
	
	var tree = new THREE.Object3D();
	var params = initialParams;
	var subStructure;	
	
	for(var i=0; i<structure.length; i++){
	// Scorro la struttura dell'albero	
		var currentChar = structure.charAt(i);
		if(	currentChar == "F" ){
		// Se trovo un ramo
			// lo creo con posizione e direzione attuali
			var newBranch = buildBranch( params, material );
			tree.add( newBranch );
			// riduco la sezione del ramo	
			var angle = Math.atan(params.branchLenght/(params.branchMaxRadius-params.branchMinRadius));
			var hip = Math.sqrt( Math.pow(params.branchLenght,2) + Math.pow(params.branchMaxRadius-params.branchMinRadius,2) )/2;
			var inc = hip*Math.cos(angle);
	
			params.branchMaxRadius = params.branchMinRadius + inc;
			params.branchMinRadius = params.branchMinRadius - params.branchMinRadius*params.branchReduction/100;
			
		}else if ( currentChar == "R" ){
		// Se trovo un ramo terminale
			//Recupero la regola di costruzione delle foglie
			leafStructure = getRandomRule(treeRules, "L");
			if(leafStructure!=""){
				// costruisco le foglie e le aggiungo all'albero attuale
				tree.add( buildTree( leafStructure, initLeafParams(params, treeType), material, leafMaterial, treeRules, treeType ) );
			}
			i = i + 1;
		}else if ( currentChar == "L" ){
		// Se trovo una foglia
			// costruisco le foglie e le aggiungo all'albero attuale
				tree.add( buildLeaf( initLeafParams(params, treeType), leafMaterial, treeType ) );
			i = i + 1;
		}else if ( currentChar == "[" ){
		// Se trovo l'inizio di un nuovo sotto albero
			// estraggo la sequenza del sotto albero
			subStructure = extractTreeSubStructure( structure, i+1 );
			// costruisco (ricorsivamente) il sottoalbero e lo aggiungo all'albero attuale
			tree.add( buildTree( subStructure, initBranchParams(params), material, leafMaterial, treeRules, treeType ) );
			// scorro in avanti la sequenza dell'albero di tanti caratteri
			// quanta è la lunghezza della sequenza del sottoalbero
			i = i + subStructure.length + 1;
		}else if ( currentChar == "+" ){
		// Se trovo una rotazione positiva lungo l'asse x
			// aggiorno la direzione in cui verranno costruiti i successivi elementi dell'albero
			params.rotation.multiply( new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3(1, 0, 0), params.branchAngle * Math.PI/180 ) );
		}else if ( currentChar == "-" ){
		// Se trovo una rotazione negativa lungo l'asse x
			// aggiorno la direzione in cui verranno costruiti i successivi elementi dell'albero
			params.rotation.multiply( new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3(1, 0, 0), -params.branchAngle * Math.PI/180 ) );
		}else if ( currentChar == "^" ){
		// Se trovo una rotazione positiva lungo l'asse y
			// aggiorno la direzione in cui verranno costruiti i successivi elementi dell'albero
			params.rotation.multiply( new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3(0, 1, 0), params.branchAngle * Math.PI/180 ) );
		}else if ( currentChar == "v" ){
		// Se trovo una rotazione negativa lungo l'asse y
			// aggiorno la direzione in cui verranno costruiti i successivi elementi dell'albero
			params.rotation.multiply( new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3(0, 1, 0), -params.branchAngle * Math.PI/180 ) );
		}else if ( currentChar == ">" ){
		// Se trovo una rotazione positiva lungo l'asse z
			// aggiorno la direzione in cui verranno costruiti i successivi elementi dell'albero
			params.rotation.multiply( new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3(0, 0, 1), params.branchAngle * Math.PI/180 ) );
		}else if ( currentChar == "<" ){
		// Se trovo una rotazione negativa lungo l'asse z
			// aggiorno la direzione in cui verranno costruiti i successivi elementi dell'albero
			params.rotation.multiply( new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3(0, 0, 1), -params.branchAngle * Math.PI/180 ) );
		}
	}
	
	return tree;	
}


function initBranchParams( fatherParams ){
	
	var branchParams = {};

	branchParams.iterations = fatherParams.iterations;
	branchParams.branchAngle = fatherParams.branchAngle;
	branchParams.branchMinRadius = fatherParams.branchMinRadius - fatherParams.branchMinRadius*fatherParams.branchReduction/100;
	branchParams.branchLenght = fatherParams.branchLenght - fatherParams.branchLenght*(fatherParams.branchReduction+10)/100;
	
	var angle = Math.atan(fatherParams.branchLenght/(fatherParams.branchMaxRadius-fatherParams.branchMinRadius));
	var hip = Math.sqrt( Math.pow(fatherParams.branchLenght,2) + Math.pow(fatherParams.branchMaxRadius-fatherParams.branchMinRadius,2) )/2;
	
	var inc = hip*Math.cos(angle);
	
	branchParams.branchMaxRadius = fatherParams.branchMinRadius + inc;
	
	branchParams.branchReduction = fatherParams.branchReduction;
	branchParams.position = new THREE.Vector3().copy(fatherParams.position);
	branchParams.rotation = new THREE.Quaternion().copy(fatherParams.rotation);
	
	return branchParams;
}


function buildBranch( params, material ){
	
	var position = new THREE.Vector3( 0.0, params.branchLenght/2, 0.0 );
	position.applyQuaternion( params.rotation );
	params.position.add(position);
	
	var branchStructure = new THREE.CylinderBufferGeometry( params.branchMinRadius, params.branchMaxRadius, params.branchLenght, 100 );
	
	var branch = new THREE.Mesh( branchStructure, material );	
	
	branch.quaternion.copy( params.rotation );
	
	branch.position.copy( params.position );
	
	return branch;
}


function initLeafParams( branchParams, treeType ){
	
	var newParams = {};
	
	if(treeType=="treeWithLeaves"){
		newParams.branchMinRadius = 0.2;
		newParams.branchMaxRadius = 0.25;
		newParams.branchLenght = 20;	
		newParams.branchReduction = 1;
		newParams.leafLength = 6;
	}else if(treeType=="firWithLeaves"){
		newParams.branchMinRadius = 1.2;
		newParams.branchMaxRadius = 1.5;
		newParams.branchLenght = 3;	
		newParams.branchReduction = 1;
		newParams.leafLength = 1.2;
	}else{
		newParams.branchMinRadius = 0.2;
		newParams.branchMaxRadius = 0.25;
		newParams.branchLenght = 20;	
		newParams.branchReduction = 1;
		newParams.leafLength = 6;
	}
	
	newParams.branchAngle = branchParams.branchAngle;
	newParams.position = new THREE.Vector3().copy(branchParams.position);
	newParams.rotation = new THREE.Quaternion().copy(branchParams.rotation);
	
	return newParams;
}


function buildLeaf( params, leafMaterial, treeType ){
	
	var position = new THREE.Vector3( 0.0, params.leafLength/2, 0.0 );
	position.applyQuaternion( params.rotation );
	params.position.add(position);
	
	if(treeType=="treeWithLeaves"){
		var leafStructure = new THREE.CircleBufferGeometry( params.leafLength/2, 12, 50, Math.PI * 2 );
		var leaf = new THREE.Mesh( leafStructure, leafMaterial );	
		leaf.quaternion.copy( params.rotation );
		leaf.position.copy( params.position );
	}else if(treeType=="firWithLeaves"){	
		var leafStructure = new THREE.CylinderBufferGeometry( params.leafLength, params.leafLength, 40, 10 );
		var leaf = new THREE.Mesh( leafStructure, leafMaterial );	
		leaf.quaternion.copy( params.rotation );
		leaf.position.copy( params.position );
	}
	
	return leaf;
}