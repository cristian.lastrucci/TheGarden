var SCREEN_WIDTH = window.innerWidth;
var SCREEN_HEIGHT = window.innerHeight;

var container, camera, scene, renderer;

var cameraControls;


initScene();

renderNaturalEnvironment();

renderScene();


function initScene() {
	// Inizializza container, camera, scena, renderer, light, events e controls
	
	container = document.createElement( 'div' );
	document.body.appendChild( container );


	// CAMERA
	camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 4000 );
	camera.position.set( -700, 700, 1600 );


	// SCENE
	scene = new THREE.Scene();
	scene.fog = new THREE.Fog( 0xffffff, 1500, 5000 );
	scene.add( camera );


	// LIGHTS
	scene.add( new THREE.AmbientLight( 0x222222 ) );

	var light = new THREE.DirectionalLight( 0xffffff, 2.25 );
	light.position.set( -200, 800, -200 );

	light.castShadow = true;

	light.shadow.mapSize.width = 1024;
	light.shadow.mapSize.height = 512;

	light.shadow.camera.near = 100;
	light.shadow.camera.far = 1200;

	light.shadow.camera.left = -1000;
	light.shadow.camera.right = 1000;
	light.shadow.camera.top = 350;
	light.shadow.camera.bottom = -350;
	
	var light2 = new THREE.DirectionalLight( 0xffffff, 2.25 );
	light2.position.set( -0, -1900, 0 );

	light2.castShadow = true;

	light2.shadow.mapSize.width = 1024;
	light2.shadow.mapSize.height = 512;

	light2.shadow.camera.near = 100;
	light2.shadow.camera.far = 1200;

	light2.shadow.camera.left = -1000;
	light2.shadow.camera.right = 1000;
	light2.shadow.camera.top = 350;
	light2.shadow.camera.bottom = -350;

	scene.add( light );
	scene.add( light2 );


	// RENDERER
	renderer = new THREE.WebGLRenderer( { antialias: true } );
	//renderer.setClearColor( scene.fog.color );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
	container.appendChild( renderer.domElement );
	renderer.gammaInput = true;
	renderer.gammaOutput = true;
	renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = THREE.PCFSoftShadowMap;


	// EVENTS
	window.addEventListener( 'resize', onWindowResize, false );


	// CONTROLS
	cameraControls = new THREE.OrbitControls( camera, renderer.domElement );
	cameraControls.target.set( 0, 50, 0 );

}


function renderNaturalEnvironment(){
	// Disegna gli elementi dell'ambiente naturale
	
	
	//  ------- SKY
	var skyTexture = new THREE.TextureLoader().load( "textures/sky7.jpg" );
	var skyMaterial = new THREE.MeshPhongMaterial( {map: skyTexture, side: THREE.DoubleSide } );
	//var sky = new THREE.Mesh( new THREE.CylinderGeometry( 2000, 2000, 4000, 100, 100 ), skyMaterial );
	var sky = new THREE.Mesh( new THREE.SphereBufferGeometry( 2000, 50, 50 ), skyMaterial );
	
	sky.position.set( 0, 0, 0 );
	sky.material.map.repeat.set( 4, 4 );
	sky.material.map.wrapS = THREE.RepeatWrapping;
	sky.material.map.wrapT = THREE.RepeatWrapping;
	sky.receiveShadow = true;
	scene.add( sky );
	
	//  ------- GROUND
	var groundTexture = new THREE.TextureLoader().load( "textures/desert.jpg" );
	var groundMaterial = new THREE.MeshPhongMaterial( {map: groundTexture, side: THREE.DoubleSide } );
	var ground = new THREE.Mesh( new THREE.CylinderGeometry( 2000, 2000, 1, 100, 100 ), groundMaterial );
	ground.rotation.x = - Math.PI / 2;
	ground.rotation.y = - Math.PI / 2;
	ground.rotation.z = - Math.PI / 2;
	ground.material.map.repeat.set( 15, 15 );
	ground.material.map.wrapS = THREE.RepeatWrapping;
	ground.material.map.wrapT = THREE.RepeatWrapping;
	ground.receiveShadow = true;
	scene.add( ground );
	
	//  ------- BENCH
	var benchTexture = new THREE.TextureLoader().load( 'textures/stone.jpg' );
	var benchMaterial = new THREE.MeshPhongMaterial( { map: benchTexture, side: THREE.DoubleSide } );
	benchMaterial.map.repeat.set( 500, 10 );
	benchMaterial.map.wrapS = THREE.RepeatWrapping;
	benchMaterial.map.wrapT = THREE.RepeatWrapping;
	
	var bench = new THREE.Mesh( new THREE.TorusGeometry( 600, 10, 100, 100, 1.06 ), benchMaterial );
	bench.position.set( 0, 7, 0 );
	bench.rotation.x = 1.57;
	bench.rotation.z = 0.255;
	bench.receiveShadow = true;
	scene.add( bench );
	
	var bench = new THREE.Mesh( new THREE.TorusGeometry( 600, 10, 100, 100, 1.06 ), benchMaterial );
	bench.position.set( 0, 7, 0 );
	bench.rotation.x = 1.57;
	bench.rotation.z = 4.968;
	bench.receiveShadow = true;
	scene.add( bench );
	
	var bench = new THREE.Mesh( new THREE.TorusGeometry( 600, 10, 100, 100, 1.06 ), benchMaterial );
	bench.position.set( 0, 7, 0 );
	bench.rotation.x = 1.57;
	bench.rotation.z = 8.11;
	bench.receiveShadow = true;
	scene.add( bench );
	
	var bench = new THREE.Mesh( new THREE.TorusGeometry( 600, 10, 100, 100, 1.06 ), benchMaterial );
	bench.position.set( 0, 7, 0 );
	bench.rotation.x = 1.57;
	bench.rotation.z = 3.3955;
	bench.receiveShadow = true;
	scene.add( bench );
	
	var bench = new THREE.Mesh( new THREE.TorusGeometry( 900, 10, 100, 100, 1.22 ), benchMaterial );
	bench.position.set( 0, 7, 0 );
	bench.rotation.x = 1.57;
	bench.rotation.z = 0.175;
	bench.receiveShadow = true;
	scene.add( bench );
	
	var bench = new THREE.Mesh( new THREE.TorusGeometry( 900, 10, 100, 100, 1.22 ), benchMaterial );
	bench.position.set( 0, 7, 0 );
	bench.rotation.x = 1.57;
	bench.rotation.z = 1.745;
	bench.receiveShadow = true;
	scene.add( bench );
	
	var bench = new THREE.Mesh( new THREE.TorusGeometry( 900, 10, 100, 100, 1.22 ), benchMaterial );
	bench.position.set( 0, 7, 0 );
	bench.rotation.x = 1.57;
	bench.rotation.z = 3.324;
	bench.receiveShadow = true;
	scene.add( bench );
	
	var bench = new THREE.Mesh( new THREE.TorusGeometry( 900, 10, 100, 100, 1.22 ), benchMaterial );
	bench.position.set( 0, 7, 0 );
	bench.rotation.x = 1.57;
	bench.rotation.z = 4.888;
	bench.receiveShadow = true;
	scene.add( bench );
	
	var wallTexture = new THREE.TextureLoader().load( 'textures/stone.jpg' );
	var wallMaterial = new THREE.MeshPhongMaterial( { map: wallTexture, side: THREE.DoubleSide } );
	wallMaterial.map.repeat.set( 1, 1 );
	wallMaterial.map.wrapS = THREE.RepeatWrapping;
	wallMaterial.map.wrapT = THREE.RepeatWrapping;
	
	var wall = new THREE.Mesh( new THREE.BoxGeometry( 1445, 20, 20.5 ), benchMaterial );
	wall.position.set( 1290, 7.5, 149 );
	wall.receiveShadow = true;
	scene.add( wall );
	
	var wall = new THREE.Mesh( new THREE.BoxGeometry( 1445, 20, 20.5 ), benchMaterial );
	wall.position.set( 1290, 8, -149 );
	wall.receiveShadow = true;
	scene.add( wall );
	
	var wall = new THREE.Mesh( new THREE.BoxGeometry( 1445, 20, 20.5 ), benchMaterial );
	wall.position.set( -1290, 8, 149 );
	wall.receiveShadow = true;
	scene.add( wall );
	
	var wall = new THREE.Mesh( new THREE.BoxGeometry( 1445, 20, 20.5 ), benchMaterial );
	wall.position.set( -1290, 8, -149 );
	wall.receiveShadow = true;
	scene.add( wall );
	
	var wall = new THREE.Mesh( new THREE.BoxGeometry( 1445, 20, 20.5 ), benchMaterial );
	wall.position.set( 149, 8, 1290 );
	wall.rotation.y = - Math.PI / 2;
	wall.receiveShadow = true;
	scene.add( wall );
	
	var wall = new THREE.Mesh( new THREE.BoxGeometry( 1445, 20, 20.5 ), benchMaterial );
	wall.position.set( -149, 7.5, 1290 );
	wall.rotation.y = - Math.PI / 2;
	wall.receiveShadow = true;
	scene.add( wall );
	
	var wall = new THREE.Mesh( new THREE.BoxGeometry( 1445, 20, 20.5 ), benchMaterial );
	wall.position.set( 149, 7.5, -1290 );
	wall.rotation.y = - Math.PI / 2;
	wall.receiveShadow = true;
	scene.add( wall );
	
	var wall = new THREE.Mesh( new THREE.BoxGeometry( 1445, 20, 20.5 ), benchMaterial );
	wall.position.set( -149, 7.5, -1290 );
	wall.rotation.y = - Math.PI / 2;
	wall.receiveShadow = true;
	scene.add( wall );
	
	//  ------- WATER
	var waterTexture = new THREE.TextureLoader().load( "textures/water2.jpg" );
	var waterMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff, map: waterTexture, side: THREE.DoubleSide } );
	var water = new THREE.Mesh( new THREE.RingGeometry( 900, 600, 100, 100, 100, Math.PI * 2 ), waterMaterial );
	water.position.set( 0, 1, 0 );
	water.rotation.x = - Math.PI / 2;
	water.rotation.z = - Math.PI / 2;
	water.material.map.repeat.set( 10, 10 );
	water.material.map.wrapS = THREE.RepeatWrapping;
	water.material.map.wrapT = THREE.RepeatWrapping;
	water.receiveShadow = true;
	scene.add( water );
	
	
	//  ------- GRASSPATH1
	var grassTexture = new THREE.TextureLoader().load( "textures/grass3.jpg" );
	var grassMaterial = new THREE.MeshPhongMaterial( { color: 0xffffff, map: grassTexture, side: THREE.DoubleSide } );
	var grassPath1 = new THREE.Mesh( new THREE.BoxBufferGeometry( 300, 4000, 2 ), grassMaterial );
	grassPath1.position.set( 0, 1, 0 );
	grassPath1.rotation.x = - Math.PI / 2;
	grassPath1.rotation.z = - Math.PI / 2;
	grassPath1.material.map.repeat.set( 3, 42 );
	grassPath1.material.map.wrapS = THREE.RepeatWrapping;
	grassPath1.material.map.wrapT = THREE.RepeatWrapping;
	grassPath1.receiveShadow = true;
	scene.add( grassPath1 );
	
	//  ------- GRASSPATH2
	var grassPath2 = new THREE.Mesh( new THREE.BoxBufferGeometry( 300, 4000, 2 ), grassMaterial );
	grassPath2.position.set( 0, 1, 0 );
	grassPath2.rotation.x = - Math.PI / 2;
	grassPath2.material.map.repeat.set( 3, 42 );
	grassPath2.material.map.wrapS = THREE.RepeatWrapping;
	grassPath2.material.map.wrapT = THREE.RepeatWrapping;
	grassPath2.receiveShadow = true;
	scene.add( grassPath2 );
	
	
	
	//  ------- SQUARE
	var squareTexture = new THREE.TextureLoader().load( 'textures/grass3.jpg' );
	var squareMaterial = new THREE.MeshPhongMaterial( { color: 0xffffff, map: squareTexture, side: THREE.DoubleSide } );
	var square = new THREE.Mesh( new THREE.CylinderGeometry( 600, 600, 2, 100, 100 ), squareMaterial );
	square.position.set( 0, 1, 0 );
	square.material.map.repeat.set(10, 10 );
	square.material.map.wrapS = THREE.RepeatWrapping;
	square.material.map.wrapT = THREE.RepeatWrapping;
	scene.add( square );
	
	//  ------- FLOWERBED
	var flowerbedTexture = new THREE.TextureLoader().load( 'textures/stone.jpg' );
	var flowerbedMaterial = new THREE.MeshPhongMaterial( { map: flowerbedTexture, side: THREE.DoubleSide } );
	var flowerbed = new THREE.Mesh( new THREE.TorusGeometry( 300, 10, 100, 100 ), flowerbedMaterial );
	flowerbed.position.set( 0, 7, 0 );
	flowerbed.rotation.x = 1.57;
	flowerbed.material.map.repeat.set( 500, 10 );
	flowerbed.material.map.wrapS = THREE.RepeatWrapping;
	flowerbed.material.map.wrapT = THREE.RepeatWrapping;
	flowerbed.receiveShadow = true;
	scene.add( flowerbed );
	
	//  ------- FLOWERS
	var flowersTexture = new THREE.TextureLoader().load( 'textures/flowers3.jpg' );
	var flowersMaterial = new THREE.MeshBasicMaterial( { map: flowersTexture, side: THREE.DoubleSide } );
	var flowers = new THREE.Mesh( new THREE.CylinderGeometry( 291, 291, 8, 100, 100 ), flowersMaterial );
	flowers.position.set( 0, 4.2, 0 );
	flowers.material.map.repeat.set( 10, 10 );
	flowers.material.map.wrapS = THREE.RepeatWrapping;
	flowers.material.map.wrapT = THREE.RepeatWrapping;
	scene.add( flowers );
	
	//  ------- WOOD
	/*var woodTexture = new THREE.TextureLoader().load( 'textures/wood.jpg' );
	var woodMaterial = new THREE.MeshBasicMaterial( { map: woodTexture } );
	var wood = new THREE.Mesh( new THREE.CylinderGeometry(5, 5, 400, 40, 5 ), woodMaterial );
	wood.position.set( 0, 200, 0 );
	scene.add( wood );*/
	
}


function onWindowResize( event ) {
	SCREEN_WIDTH = window.innerWidth;
	SCREEN_HEIGHT = window.innerHeight;

	renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );

	camera.aspect = SCREEN_WIDTH/ SCREEN_HEIGHT;
	camera.updateProjectionMatrix();
}


function renderScene() {
	requestAnimationFrame( renderScene );
	renderer.render( scene, camera );
}


function addToScene( object ){
	scene.add( object );
	renderScene();	
}


function removeToScene( object ){
	scene.remove( object );
	renderScene();	
}