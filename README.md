This simple Javascript API implements, with WebGL functions, the [L-System](https://en.wikipedia.org/wiki/L-system) algorithm for tree generation in a 3D scene.<br />
Currently is available a small set of trees, visible [here](http://cristian.lastrucci.gitlab.io/TheGarden/), but is also possible extend it setting the new trees paramethers in [treeBuilder.js](http://gitlab.com/cristian.lastrucci/TheGarden/blob/master/js/treeBuilder.js/)

[Here](/projectPresentation.pdf) is available a presentation of the Project. 